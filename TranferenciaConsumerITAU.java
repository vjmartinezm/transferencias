package py.edu.ucom.is2.proyectocamel.transferencias;




import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TranferenciaConsumerITAU extends RouteBuilder{

	private JacksonDataFormat jsonDataFormat;
	
	@Autowired
	TransferenciaService service;
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		
		jsonDataFormat = new JacksonDataFormat(TransferenciaRequest.class);

	
		from("activemq:Martinez-ITAU-IN")
		.log("Consumidor ITAU-IN")
		.unmarshal(jsonDataFormat)
		.process(new TransferenciaAgregarHeaderProcessor())
		.filter().method(FiltroTransferencia.class,"validarFecha")
			.to("direct:procesarSwitchAceptado").stop()
			.end()

		.to("direct:procesarSwitchRechazado").stop()
		.end();
			
		from("direct:procesarSwitchAceptado")
		.log("Validacion Fechas Aceptados")
			.bean(service,"TransferenciaAceptada")
			.choice()
			.when(header("banco_origen").contains("ITAU"))
			.log("Banco origen ITAU")
			.setExchangePattern(ExchangePattern.InOnly)
			.marshal(jsonDataFormat)
			.to("activemq:Martinez-ITAU-OUT")
			.endChoice()
			.when(header("banco_origen").contains("ATLAS"))
				.log("Banco origen ATLAS")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ATLAS-OUT")
				.endChoice()
			.when(header("banco_origen").contains("FAMILIAR"))
				.log("Banco origen FAMILIAR")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-FAMILIAR-OUT")
				.endChoice()
			.otherwise()
				.log("Ninguna de las opciones")
			.end();
			
		from("direct:procesarSwitchRechazado")
		.log("Validacion Fechas Rechazado")
			.bean(service,"TransferenciaRechazada")
			.choice()
			.when(header("banco_origen").contains("ATLAS"))
				.log("Banco origen ATLAS")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ATLAS-OUT")
				.endChoice()
			.when(header("banco_origen").contains("ITAU"))
				.log("Banco origen ITAU")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-ITAU-OUT")
				.endChoice()
			.when(header("banco_origen").contains("FAMILIAR"))
				.log("Banco origen FAMILIAR")
				.setExchangePattern(ExchangePattern.InOnly)
				.marshal(jsonDataFormat)
				.to("activemq:Martinez-FAMILIAR-OUT")
				.endChoice()	
			.otherwise()
				.log("Ninguna de las opciones")
			.end();
				
	}

	

	
	

}
