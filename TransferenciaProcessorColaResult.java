package py.edu.ucom.is2.proyectocamel.transferencias;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


@Component
public class TransferenciaProcessorColaResult implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		TransferenciaResponse transferenciaRequest = exchange.getIn().getBody(TransferenciaResponse.class);
		//System.out.println(bancoRequest);
				
		if(transferenciaRequest != null) {
			
			
			System.out.println("Resultado Transferencia ID: " +transferenciaRequest.getIdTransaccion() + 
					" --> " +transferenciaRequest.getMensaje());


			
		}
		else
			System.err.print("bancoService NULL");
	}
}
