package py.edu.ucom.is2.proyectocamel.transferencias;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import org.springframework.stereotype.Component;

@Component
public class FiltroTransferencia {
	public boolean validarMonto(TransferenciaRequest transferenciaRequest) {
	
		if (transferenciaRequest.getMonto() < 20000000) {
			return true;
		}else {
			return false;
		}
		
	}
	 
	public boolean validarFecha(TransferenciaRequest transferenciaRequest) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		
		String  requestDate = transferenciaRequest.getFecha();
		
		LocalDate myDate = LocalDate.parse(requestDate,dtf);

	    LocalDate currentDate = LocalDate.now();
		
		
		long numeroOFDays = ChronoUnit.DAYS.between(myDate,currentDate);
		
		if (dtf.format(now).equals(transferenciaRequest.getFecha())) {
			return true;
		}else if(numeroOFDays < 2){
			return true;
		}else {
			return false;
		}
		

		
	}
	
	

}
